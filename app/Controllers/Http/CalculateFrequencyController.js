'use strict'

const axios = use("axios");
const Config = use('Config');
const _ = use('lodash');

class CalculateFrequencyController {
  async calculate( {request} ) {
    const {length} = request.all();
    const contents = await this.getContents()

    let wordsCount = {};

    contents.split("\n").forEach((lines) => {
      if (lines.trim().length > 0) {
        wordsCount = this.processContents(lines.split(" "), wordsCount);
      }
    })

    let wordsCountSorted = this.sortWordCount(wordsCount);

    if (length && length > 0) {
      wordsCountSorted = _.slice(wordsCountSorted, 0, length)
    }

    return {
      wordsCountSorted
    };
  }

  /*
    function to retrieve the contents from the URL
    return: string
  */
  async getContents() {
    let content;

    await axios({
      method: 'get',
      url: Config.get("app.content_url"),
    })
      .then((response) => {
        content = response.data;
      });

    return content;
  }

  /*
    Process the content and calculates the frequency of words
  */
  processContents(contentsArr, wordsCount) {
    let stopWords = ['the', 'a', 'of', 'and', 'in', 'is', 'to'];

    let count = 0;

    contentsArr.forEach(element => {
      element = element.trim().replace(/\W/g, '').toLowerCase();

      if (element.length > 0) {
        /* if element is not in stopWords */
        if (stopWords.indexOf(element) == -1) {
          if (wordsCount[element] && wordsCount[element] >= 0) {
            wordsCount[element] ++;
          } else {
            wordsCount[element] = 1;
          }
        }
      }

    });

    return wordsCount;
  }

  sortWordCount(wordsCount) {

    let wordsCountSorted = []
    _.each(wordsCount, (element, key) => {
      wordsCountSorted.push({
        word: key,
        count: element
      });
    });

    wordsCountSorted = _.sortBy(wordsCountSorted, [ (o) => {
      return o.count;
    }]);

    return wordsCountSorted.reverse();

  }
}

module.exports = CalculateFrequencyController
