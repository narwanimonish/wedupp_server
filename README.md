# WedUpp Test (Server Application)

This application is build in NodeJs using AdonisJs boilerplate. AdonisJs comes with great commandline tool & ease to use database operations.

This app contains POST Route "/api/frequency" which retrieves data & calculates the count of occurances of words in the text & returns N-most used words.
There is a controller defined "app/Controllers/Http/CalculateFrequencyController.js" which has all the business logic defined.
It performs following tasks:

1. Get the lenght parameter from the request object
2. Get the contents from the URL: https://gitlab.com/snippets/1824628/raw
3. Calculates the count of words
4. Return the N words sorted in descending

Libraries used:

1. Axios - To retrieve contents
2. Lodash - To sort the word count frequency

